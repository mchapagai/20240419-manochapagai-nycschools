package com.mchapagai.nycschools.service;

import android.support.test.filters.LargeTest;

import androidx.annotation.NonNull;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.mchapagai.nycschools.model.SchoolsResponse;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@RunWith(AndroidJUnit4ClassRunner.class)
@LargeTest
public class SchoolsServiceTest {
    final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();


    @Test
    public void tesNYCSchoolApiResultsDetails() {

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        SchoolsService requestInterface = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(SchoolsService.class);

        requestInterface.fetchSchools()
                .enqueue(new Callback<ArrayList<SchoolsResponse>>() {
                    @Override
                    public void onResponse(@NonNull Call<ArrayList<SchoolsResponse>> call, Response<ArrayList<SchoolsResponse>> response) {
                        Assert.assertNotNull(response.body());

                    }

                    @Override
                    public void onFailure(Call<ArrayList<SchoolsResponse>> call, Throwable t) {
                    }
                });


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
