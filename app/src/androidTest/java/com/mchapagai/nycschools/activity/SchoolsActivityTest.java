package com.mchapagai.nycschools.activity;

import android.content.Intent;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class SchoolsActivityTest {

    @Rule
    public ActivityTestRule<SchoolsActivity> rule = new ActivityTestRule<>(
            SchoolsActivity.class);
    @Test
    public void launchActivity() throws InterruptedException {
        Intent intent = new Intent();
       SchoolsActivity schoolsActivity = rule.launchActivity(intent);

       Thread.sleep(3000);
        assertEquals("com.mchapagai.nycschools", schoolsActivity.getPackageName());

    }

}
