package com.mchapagai.nycschools.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mchapagai.nycschools.model.SchoolDetailsResponse;
import com.mchapagai.nycschools.model.SchoolsResponse;
import com.mchapagai.nycschools.api.SchoolsAPI;

import java.util.ArrayList;
import java.util.List;


public class SchoolsViewModel extends AndroidViewModel {

    private final SchoolsAPI schoolsAPI;
    private final MutableLiveData<ArrayList<SchoolsResponse>> schoolResponseLiveData;

    public SchoolsViewModel(@NonNull Application application) {
        super(application);
        schoolsAPI = new SchoolsAPI();
        this.schoolResponseLiveData = schoolsAPI.fetchSchools();
    }

    public LiveData<ArrayList<SchoolsResponse>> fetchSchools() {
        return schoolResponseLiveData;
    }

    public LiveData<List<SchoolDetailsResponse>> fetchSATDetails(String dbn) {
        return schoolsAPI.fetchSATDetails(dbn);
    }

}
