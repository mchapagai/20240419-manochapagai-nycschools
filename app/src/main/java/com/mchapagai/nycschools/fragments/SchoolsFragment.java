package com.mchapagai.nycschools.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mchapagai.nycschools.R;
import com.mchapagai.nycschools.activity.SchoolDetailsActivity;
import com.mchapagai.nycschools.adapter.SchoolsAdapter;
import com.mchapagai.nycschools.model.SchoolsResponse;
import com.mchapagai.nycschools.view.LoadingIndicator;
import com.mchapagai.nycschools.viewmodel.SchoolsViewModel;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SchoolsFragment extends Fragment implements SchoolsAdapter.OnSchoolItemClicked {
    RecyclerView recyclerView;
    TextView emptyViewContainer;
    private View rootView;
    private SchoolsAdapter schoolsAdapter;
    private SchoolsViewModel schoolsViewModel;

    public static SchoolsFragment newInstance() {
        return new SchoolsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.school_list_container, container, false);
            recyclerView = rootView.findViewById(R.id.school_recycler_view);
            emptyViewContainer = rootView.findViewById(R.id.empty_view);
            initRecyclerView();
            schoolsViewModel = ViewModelProviders.of(this).get(SchoolsViewModel.class);
            fetchSchools();
        }


        return rootView;
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        schoolsAdapter = new SchoolsAdapter(new ArrayList<>(), this);
        recyclerView.setAdapter(schoolsAdapter);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    private void fetchSchools() {

        LoadingIndicator.showProgressDialog(getActivity(), "Loading...");
        schoolsViewModel.fetchSchools().observe(getViewLifecycleOwner(), nycSchoolsList -> {

            LoadingIndicator.dismissDialog();
            if (nycSchoolsList != null) {
                if (nycSchoolsList.size() > 0) {
                    schoolsAdapter = new SchoolsAdapter(nycSchoolsList, this);
                    schoolsAdapter.notifyDataSetChanged();
                    recyclerView.setAdapter(schoolsAdapter);
                    LoadingIndicator.dismissDialog();
                    emptyViewContainer.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    LoadingIndicator.dismissDialog();
                    recyclerView.setVisibility(View.GONE);
                    emptyViewContainer.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSchoolItemClicked(SchoolsResponse response) {
        Intent intent = new Intent(getActivity(), SchoolDetailsActivity.class);
        intent.putExtra("name", response.getSchool_name());
        intent.putExtra("school_dbn",response.getDbn());
        startActivity(intent);
    }
}
