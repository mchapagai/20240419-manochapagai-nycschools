package com.mchapagai.nycschools.api;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.mchapagai.nycschools.model.SchoolDetailsResponse;
import com.mchapagai.nycschools.model.SchoolsResponse;
import com.mchapagai.nycschools.service.SchoolsService;
import com.mchapagai.nycschools.service.ServiceFactory;


import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolsAPI {
    private final SchoolsService schoolsService;

    public SchoolsAPI() {
        schoolsService = ServiceFactory.createService(SchoolsService.class);
    }

    public MutableLiveData<ArrayList<SchoolsResponse>> fetchSchools() {
        final MutableLiveData<ArrayList<SchoolsResponse>> data = new MutableLiveData<>();

        schoolsService.fetchSchools().enqueue(new Callback<ArrayList<SchoolsResponse>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<SchoolsResponse>> call, @NonNull Response<ArrayList<SchoolsResponse>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("NYCSchoolRepository", t.getMessage());
            }
        });
        return data;
    }

    public MutableLiveData<List<SchoolDetailsResponse>> fetchSATDetails(String dbn) {
        final MutableLiveData<List<SchoolDetailsResponse>> data = new MutableLiveData<>();

        schoolsService.fetchSATDetails(dbn).enqueue(new Callback<List<SchoolDetailsResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<SchoolDetailsResponse>> call, @NonNull Response<List<SchoolDetailsResponse>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.i("NYCSchoolRepository", t.getMessage());
            }
        });
        return data;
    }
}
