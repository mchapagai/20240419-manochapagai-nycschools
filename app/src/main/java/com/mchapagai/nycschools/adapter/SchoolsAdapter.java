package com.mchapagai.nycschools.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mchapagai.nycschools.R;
import com.mchapagai.nycschools.databinding.SchoolItemsContainerBinding;
import com.mchapagai.nycschools.model.SchoolsResponse;
import com.mchapagai.nycschools.BR;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.ViewHolder> {

    private final ArrayList<SchoolsResponse> schoolsResponse;
    private final OnSchoolItemClicked itemClicked;

    public SchoolsAdapter(ArrayList<SchoolsResponse> schools,
                          OnSchoolItemClicked itemClicked) {
        schoolsResponse = schools;
        this.itemClicked = itemClicked;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {

        SchoolItemsContainerBinding schools = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.school_items_container, parent, false);
        return new ViewHolder(schools);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SchoolsResponse schoolItems = schoolsResponse.get(position);
        holder.bind(schoolItems);
        holder.schoolsBinding.schoolname.setText(schoolItems.getSchool_name());

        holder.schoolsBinding.getRoot().setOnClickListener(v -> itemClicked.onSchoolItemClicked(schoolItems));
    }

    @Override
    public int getItemCount() {
        if (schoolsResponse == null || schoolsResponse.size() == 0) {
            return 0;
        }
        return Math.min(schoolsResponse.size(), 20);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        SchoolItemsContainerBinding schoolsBinding;
        public ViewHolder(SchoolItemsContainerBinding schoolsBinding) {
            super(schoolsBinding.getRoot());
            this.schoolsBinding = schoolsBinding;
        }

        public void bind(SchoolsResponse response) {
            schoolsBinding.setVariable(BR.schoolsResponse, response);
            schoolsBinding.executePendingBindings();
        }


    }

    public interface OnSchoolItemClicked {
        void onSchoolItemClicked(SchoolsResponse response);
    }
}
