package com.mchapagai.nycschools.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.mchapagai.nycschools.R;
import com.mchapagai.nycschools.fragments.SchoolsFragment;

public class SchoolsActivity extends AppCompatActivity {

    SchoolsFragment schoolsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schools_activity_container);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowTitleEnabled(true);
        schoolFragment();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void schoolFragment() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        schoolsFragment = SchoolsFragment.newInstance();
        ft.replace(R.id.school_container, schoolsFragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {

        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

}
