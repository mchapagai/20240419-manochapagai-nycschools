package com.mchapagai.nycschools.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.mchapagai.nycschools.R;
import com.mchapagai.nycschools.model.SchoolDetailsResponse;
import com.mchapagai.nycschools.viewmodel.SchoolsViewModel;

import java.util.ArrayList;

public class SchoolDetailsActivity extends AppCompatActivity {

    private TextView mathTextView, readingTextView, writingTextView, detailName;
    private final ArrayList<SchoolDetailsResponse> detailArrayList = new ArrayList<>();
    SchoolsViewModel detailViewModel;
    String name = "";
    private TextView emptyView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schools_list_details);
        detailViewModel = ViewModelProviders.of(this).get(SchoolsViewModel.class);

        mathTextView = findViewById(R.id.math_tv);
        readingTextView = findViewById(R.id.reading_tv);
        writingTextView = findViewById(R.id.writeId);
        detailName = findViewById(R.id.school_name_tv);
        emptyView = findViewById(R.id.empty_view);


        String dbn = getIntent().getStringExtra("school_dbn");
        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        detailName.setText(name);
        getDetails(dbn);
    }

    private void getDetails(String dbn) {
        detailViewModel.fetchSATDetails(dbn).observe(this, fetchResponse -> {
            if (fetchResponse != null && fetchResponse.size() > 0) {
                detailArrayList.addAll(fetchResponse);
                SchoolDetailsResponse sd = detailArrayList.get(0);
                readingTextView.setText(sd.getSat_critical_reading_avg_score());
                mathTextView.setText(sd.getSat_math_avg_score());
                writingTextView.setText(sd.getSat_writing_avg_score());
                emptyView.setVisibility(View.GONE);
            } else {
                emptyView.setVisibility(View.VISIBLE);
            }
        });
    }


}
