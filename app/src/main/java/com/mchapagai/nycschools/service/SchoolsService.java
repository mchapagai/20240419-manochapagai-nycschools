package com.mchapagai.nycschools.service;

import com.mchapagai.nycschools.model.SchoolDetailsResponse;
import com.mchapagai.nycschools.model.SchoolsResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolsService {

    @GET("s3k6-pzi2.json")
    Call<ArrayList<SchoolsResponse>> fetchSchools();

    @GET("f9bf-2cp4.json")
    Call<List<SchoolDetailsResponse>> fetchSATDetails(@Query("dbn") String dbn);

}
