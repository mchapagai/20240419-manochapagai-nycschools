package com.mchapagai.nycschools.view;

import android.app.ProgressDialog;
import android.content.Context;

public class LoadingIndicator {

    private static ProgressDialog dialog;

    public static void showProgressDialog(Context context, String message) {
        dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        dialog.setProgressStyle((ProgressDialog.STYLE_SPINNER));
        dialog.show();
    }

    public static void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
